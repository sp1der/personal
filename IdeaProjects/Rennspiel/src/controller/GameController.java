package controller;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import model.*;
import view.GameView;

import java.util.ArrayList;

public class GameController {

    private GameModel gameModel;
    private GameView gameView;
    private Scene scene;
    private ArrayList<KeyCode> input;
    private boolean paused = false;

    public GameController(GameModel gameModel, GameView gameView) {
        this.gameView = gameView;
        this.gameModel = gameModel;
        this.scene = gameView.getScene();
        this.input = new ArrayList<>();
        //Set up keylistener
        setUpInputHandler();
    }

    /**
     * Updates all needed dependencies every frame
     *
     * @param timeDifferenceInSeconds the time passed since last frame
     */
    public void updateContinuously(double timeDifferenceInSeconds) {
        Car car = gameModel.getCar();
        if(!paused) {
            if(input.contains(KeyCode.P)) {
                gameView.switchPause(paused);
                paused = true;
                return;
            }
            if(input.contains(KeyCode.UP)) {
                car.accelerate(timeDifferenceInSeconds);
            }
            if(input.contains(KeyCode.DOWN)) {
                car.decelerate(timeDifferenceInSeconds);
            }
            if(input.contains(KeyCode.LEFT)) {
                car.steerLeft();
            }
            if(input.contains(KeyCode.RIGHT)) {
                car.steerRight();
            }
            car.applyNaturalForces();
            car.updatePosition(timeDifferenceInSeconds);
            gameView.updateCar(car.getCurrentPosition(), car.getAngle());
        }
        else if(input.contains(KeyCode.P)) {
            gameView.switchPause(paused);
            paused = false;
        }
    }

    private void setUpInputHandler() {
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(!input.contains(event.getCode()))
                    input.add(event.getCode());
            }
        });
        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                input.remove(event.getCode());
            }
        });
    }
}
