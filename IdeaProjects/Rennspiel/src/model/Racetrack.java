package model;

import static java.lang.Math.*;

public class Racetrack {
    public static final double ROLLING_RESITANCE_STREET = 0.015;
    public static final double ROLLING_RESISTANCE_GROUND = 0.05;
    public static final double AIR_DENSITY = 1.2041;

    public static final double MAP_WIDTH = 130;
    public static final double MAP_HEIGHT = 80;
    public static final double OUTER_RADIUS_X = 55;
    public static final double OUTER_RADIUS_Y = 35;
    public static final double INNER_RADIUS_X = 45;
    public static final double INNER_RADIUS_Y = 25;

    public static final Vector center = new Vector(130/2, 80/2);
    public static final Vector initialCarPos = new Vector(center.getX()-1-Car.CAR_LENGTH/2,center.getY()-INNER_RADIUS_Y-((OUTER_RADIUS_Y-INNER_RADIUS_Y)/2));

    /**
     * checks if car has passed the checkpoint line
     * @param a vector to check
     * @return if the car has passed the checkpoint line
     */
    public static boolean afterCheckpoint(Vector a) {
        return a.angleToXAxis()>90 && isInBottomPart(a);
    }

    /**
     * checks if car has passed the start/finish line
     * @param a vector to check
     * @return if the car has passed the start/finish line
     */
    public static boolean afterFinishLine(Vector a) {
        return a.angleToXAxis()<90 && !isInBottomPart(a);
    }

    /**
     * determines if the position described by Vector a is on the race track
     * @param a vector to check
     * @return if the vector's on the race track
     */
    public static boolean onRaceTrack(Vector a) {
        double angle = a.angleToXAxis(); //angle to x-axis
        double max = Math.sqrt(pow(OUTER_RADIUS_X,2)*pow(cos(angle),2)+pow(OUTER_RADIUS_Y,2)*pow(sin(angle),2)); //max distance from center
        double min = Math.sqrt(pow(INNER_RADIUS_X,2)*pow(cos(angle),2)+pow(INNER_RADIUS_Y,2)*pow(sin(angle),2)); //min distance from center
        double distance = a.distanceTo(center);
        return distance >= max && distance <= min;
    }

    /**
     * @param a vector to check
     * @return true if the point is in the bottom part of the map
     */
    public static boolean isInBottomPart(Vector a) {
        return a.getY() > (GameModel.meterToPixel(MAP_HEIGHT)/2);
    }
}
