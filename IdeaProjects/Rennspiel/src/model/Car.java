package model;

import java.awt.Point;

/**
 * Class car represents the race-car in the race game.
 */
public class Car {
    private static final double AIR_DRAG_COEFFICIENT = 0.28;
    private static final double ACCELERATION = 1;
    private static final double ROTATION_SPEED = 0.0004;
    public static final double CAR_LENGTH = 4.255;
    public static final double CAR_WIDTH = 2.027;
    private static final double CAR_FRONT_SURFACE = 2.19;

    private double weight;
    private Vector currentPosition;
    private Vector direction;
    private boolean totaled;
    private double speed;
    private double angle = 0;
    private boolean passedCheckpoint = false;

    public Car(Vector initialPosition) {
        this.currentPosition = initialPosition;
        this.direction = new Vector(1,0);
    }
    public double getSpeed() {
        return speed;
    }
    public Vector getCurrentPosition() {
        return currentPosition;
    }
    /**
     *
     * @return if the car is totaled or operational
     */
    public boolean getTotaled() {
        return totaled;
    }

    /**
     *
     * @return angle between direction and x-axis
     */
    public double getAngle() {
        return angle;
    }
    /**
     *
     * @return the rotation of the car in radians
     */
    public double getRotation() {
        return direction.angleToXAxis();
    }
    /**
     * applies current speed and direction to the car's position
     */
    public void updatePosition(double timeSinceLastFrame) {
        Vector apply = direction.copy();
        apply.normalize();
        apply.scalarMultiply(speed*timeSinceLastFrame);
        currentPosition.add(apply);
        angle = direction.angleToXAxis();
        //System.out.println(direction.toString());
        //System.out.println(currentPosition.toString());
    }
    /**
     * applies a constant acceleration to the car
     */
    public void accelerate(double timeSinceLastFrame) {
        speed += ACCELERATION*timeSinceLastFrame;
    }
    /**
     * decelerates the car
     */
    public void decelerate(double timeSinceLastFrame) {
        double res = speed - ACCELERATION*timeSinceLastFrame*2;
        if(res>0)
            speed = res;
    }

    /**
     * applies air and drag resistance to the car
     */
    public void applyNaturalForces() {

    }
    public void steerLeft() {
        if(speed>0) {
            direction.rotate(-ROTATION_SPEED);
        }
    }
    public void steerRight() {
        if(speed>0) {
            direction.rotate(ROTATION_SPEED);
        }
    }
    public void collide() {
        if(speed > GameModel.SPEED_TOTAL_LOSS)
            totaled = true;
        else
            speed = 0;
    }
}
