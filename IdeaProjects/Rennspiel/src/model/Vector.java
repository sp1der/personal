package model;

import static java.lang.Math.*;

public class Vector {
    private static Vector xAxis = new Vector(1,0);
    private static Vector yAxis = new Vector(0,1);

    private double x;
    private double y;

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @return a copy of this vector
     */
    public Vector copy() {
        return new Vector(x,y);
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public void setX(double x) {
        this.x = x;
    }
    public void setY(double y) {
        this.y = y;
    }
    public double abs() {
        return sqrt(x*x+y*y);
    }
    public void scalarMultiply(double scalar) {
        this.x *= scalar;
        this.y *= scalar;
    }
    public void negate() {
        scalarMultiply(-1);
    }
    /**
     * computes sum of this and a
     * @param a vector to add
     */
    public void add(Vector a) {
        scaledAdd(1, a);
    }
    public void scaledAdd(double factor, Vector a) {
        x += factor*a.x;
        y += factor*a.y;
    }
    public void normalize() {
        scalarMultiply(1/abs());
    }

    /**
     * rotates vector by specified angle
     * @param angle angle to rotate vector in radians
     */
    public void rotate(double angle) {
        double xPrev = x;
        double yPrev = y;
        x = xPrev * cos(angle) - yPrev * sin(angle);
        y = xPrev * sin(angle) + yPrev * cos(angle);
    }
    /**
     * @param a second factor for multiplication
     * @return dot product between this and a
     */
    public double dotProduct(Vector a) {
        return x*a.x + y*a.y;
    }

    /**
     * @return angle between this and xAxis
     */
    public double angleToXAxis() {
        return acos(dotProduct(xAxis)/abs()*xAxis.abs());
    }

    /**
     * computes distance between this and a
     * @param a second vector
     */
    public double distanceTo(Vector a) {
        return sqrt(pow(x-a.x,2)+pow(y-a.y,2));
    }
    public String toString() {
        return String.format("(%f,%f)", x, y);
    }

    /**
     * converts radians to degrees
     * @param angle angle in radians
     * @return angle in degrees
     */
    public static double toDegrees(double angle) {
        return (360/2*PI)*angle;
    }

    /**
     * converts degress to radians
     * @param angle angle in degrees
     * @return angle in radians
     */
    public static double toRadians(double angle) {
        return (2*PI/360)*angle;
    }
    public static void main(String... args) {
        Vector test = new Vector(0,1);
        test.rotate(PI/4);
        System.out.println(test.toString());
    }

}
