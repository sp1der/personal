package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import model.GameModel;
import model.Racetrack;
import model.Vector;

import java.awt.geom.AffineTransform;
import java.io.FileInputStream;
import java.io.IOException;
import static model.GameModel.*;

/**
 * Contains every GUI element
 */
public class GameView {

    //The scene where all is stacked up
    private Scene scene;

    //Stackpane, where all dialogs are stacked
    private StackPane rootPane;

    private Pane gamePane;
    private Pane menuPane;
    private Pane pausePane;

    //Map Elements
    private Ellipse outerTrack;
    private Ellipse innerTrack;
    private Rectangle startLine;
    private Rectangle checkpointLine;
    private Rectangle car;

    //Textures
    Image grass = null;
    Image street = null;
    Image sLine = null;
    Image cLine = null;
    Image carTex = null;

    //Help values
    private double prevAngle = 0; // previous angle in degrees

    public Scene getScene() {
        return scene;
    }


    /**
     * GameView object for setting up the GUI
     *
     * @param stage the primary stage
     */
    public GameView(Stage stage) {

        stage.setTitle("Rennspiel");
        stage.setResizable(false);
        stage.sizeToScene();

        rootPane = new StackPane();
        scene = new Scene(rootPane, 1300, 800);

        setUpGameWindow();

        stage.setScene(scene);
    }

    /**
     * Sets up the main game window with the course as panebackground,
     * the car in the initial Position
     */
    private void setUpGameWindow() {
        //seperate Pane for the actual game
        gamePane = new Pane();
        drawGame();
        rootPane.getChildren().add(gamePane);
        //Pane for the game's menu
        menuPane = new Pane();
        menuPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Button start = new Button("START");
        start.setOnAction((ActionEvent event) -> {
                gamePane.toFront();
        });
        menuPane.getChildren().add(start);
        rootPane.getChildren().add(menuPane);
        pausePane = new Pane();
        pausePane.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    /**
     * brings up pause screen or switches back to game.
     * @param paused game paused or to be paused.
     */
    public void switchPause(boolean paused) {
        if(paused) {
            gamePane.toFront();
        } else {
            pausePane.toFront();
        }
    }
    public void drawGame() {
        try {
            grass = new Image(new FileInputStream("src/resources/grass.jpg"));
            street = new Image(new FileInputStream("src/resources/street.jpg"));
            sLine = new Image(new FileInputStream("src/resources/startLine.png"));
            cLine = new Image(new FileInputStream("src/resources/checkpointLine.png"));
            carTex = new Image(new FileInputStream("src/resources/car.png"));
        } catch(IOException ex) {
            ex.printStackTrace();
        }
        gamePane.setBackground(new Background(new BackgroundImage(grass, null, null, null, null)));
        outerTrack = new Ellipse();
        outerTrack.setCenterX(scene.getWidth()/2);
        outerTrack.setCenterY(scene.getHeight()/2);
        outerTrack.setRadiusX(meterToPixel(Racetrack.OUTER_RADIUS_X));
        outerTrack.setRadiusY(meterToPixel(Racetrack.OUTER_RADIUS_Y));
        outerTrack.setFill(new ImagePattern(street, 0, 0, 32, 32, false));
        innerTrack = new Ellipse();
        innerTrack.setCenterX(scene.getWidth()/2);
        innerTrack.setCenterY(scene.getHeight()/2);
        innerTrack.setRadiusX(meterToPixel(Racetrack.INNER_RADIUS_X));
        innerTrack.setRadiusY(meterToPixel(Racetrack.INNER_RADIUS_Y));
        innerTrack.setFill(new ImagePattern(grass, 0, 0, 32, 32, false));
        startLine = new Rectangle();
        startLine.setX((scene.getWidth()/2)-5);
        startLine.setY((scene.getHeight()/2)-meterToPixel(Racetrack.OUTER_RADIUS_Y));
        startLine.setHeight(meterToPixel(Racetrack.OUTER_RADIUS_Y-Racetrack.INNER_RADIUS_Y));
        startLine.setWidth(10);
        startLine.setFill(new ImagePattern(sLine, 0, 0, 10, 100 ,false));
        checkpointLine = new Rectangle();
        checkpointLine.setX((scene.getWidth()/2)-5);
        checkpointLine.setY((scene.getHeight()/2)+meterToPixel(Racetrack.INNER_RADIUS_Y));
        checkpointLine.setHeight(meterToPixel(Racetrack.OUTER_RADIUS_Y-Racetrack.INNER_RADIUS_Y));
        checkpointLine.setWidth(10);
        checkpointLine.setFill(new ImagePattern(cLine, 0, 0, 10, 100 ,false));
        car = new Rectangle();
        car.setHeight(20);
        car.setWidth(43);
        car.setX(meterToPixel(Racetrack.initialCarPos.getX())-car.getWidth()/2);
        car.setY(meterToPixel(Racetrack.initialCarPos.getY())-car.getHeight()/2);
        car.setFill(new ImagePattern(carTex, 0, 0, 43, 20 ,false));
        gamePane.getChildren().add(outerTrack);
        gamePane.getChildren().add(innerTrack);
        gamePane.getChildren().add(startLine);
        gamePane.getChildren().add(checkpointLine);
        gamePane.getChildren().add(car);
    }
    public void updateCar(Vector carPos, double angle) {
        /*double angleDegrees = Vector.toDegrees(angle);
        if(angleDegrees != prevAngle) {
            prevAngle = angleDegrees;
            car.getTransforms().add(new Rotate(angle, scene.getWidth()/2, scene.getHeight()/2));
        }*/
        car.setX(meterToPixel(carPos.getX())-car.getWidth()/2);
        car.setY(meterToPixel(carPos.getY())-car.getHeight()/2);
    }
}